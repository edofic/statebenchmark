import scala.util.Try

class UnsafeState[A]{
  var state: A = _
  def get = state
  def set(a: A){
    state = a
  }
}

object UnsafeStateBench {  
  def main(args: Array[String]){
    val n = Try(args(0).toInt) getOrElse 1000
    val m = Try(args(1).toInt) getOrElse 3
    for(_<-1 to m) bench(n)
  }
  
  def bench(n: Int){
    var state = new UnsafeState[Int]
    val start = System.currentTimeMillis()
    var i = 0
    var eqs = 0
    while(i<n){
      state.set(i)
      if(state.get == i) eqs+=1
      i+=1
    }
    val took = System.currentTimeMillis() - start
    val perIter = took.toDouble / n * 1000
    val wrong = n-eqs
    println(s"took $took ms\nper iter $perIter ns\ngot $wrong wrong")
  }
}

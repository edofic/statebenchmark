import akka.actor._
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.Await
import scala.reflect.ClassTag
import scala.concurrent.duration._
import scala.util.Try

class MutableStateActor extends Actor {
  import MutableStateActor._
  var state: Any = _

  def receive = {
    case Set(a) => state = a
    case Get => sender ! state
  }
}

object MutableStateActor {
  case class Set(a: Any)
  case object Get

  class Wrapper[A] private[MutableStateActor] (state: ActorRef)(implicit tag: ClassTag[A]){
    def set(a: A){
      state ! Set(a)
    }

    def getF(implicit timeout: Timeout) = (state ? Get).mapTo[A]

    def get(implicit timeout: Timeout) = Await.result(getF, Duration.Inf)
  }

  def apply[A](factory: ActorRefFactory)(implicit tag: ClassTag[A]): Wrapper[A] = new Wrapper(factory.actorOf(Props[MutableStateActor]))

  def apply[A](factory: ActorRefFactory, start: A)(implicit tag: ClassTag[A]): Wrapper[A] = {
    val wrapper = apply[A](factory)
    wrapper.set(start)
    wrapper
  }
}

object MutableStateActorBench {  
  def main(args: Array[String]){
    val n = Try(args(0).toInt) getOrElse 1000
    val m = Try(args(1).toInt) getOrElse 3
    for(_<-1 to m) bench(n)
  }
  
  def bench(n: Int){
    val sys = ActorSystem()
    val state = MutableStateActor(sys, 0)
    implicit val timeout = Timeout(100.millis)
    val start = System.currentTimeMillis()
    var i = 0
    var eqs = 0
    while(i<n){
      state.set(i)
      if(state.get == i) eqs+=1
      i+=1
    }
    val took = System.currentTimeMillis() - start
    val perIter = took.toDouble / n * 1000
    val wrong = n-eqs
    println(s"took $took ms\nper iter $perIter ns\ngot $wrong wrong")
    sys.shutdown()
  }
}
